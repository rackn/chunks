package chunks

import (
	"encoding/binary"
	"math/rand"
	"path"
	"testing"
)

func TestChunkOps(t *testing.T) {
	tmpDir := t.TempDir()
	storeFile := path.Join(tmpDir, "a.log")
	cs, err := Open(storeFile, false)
	if err != nil {
		t.Fatalf("Error creating new chunk store: %v", err)
	}
	// Make some random data to write
	buf := make([]byte, 65536)
	r := rand.New(rand.NewSource(7))
	for i := 0; i < len(buf); i += 8 {
		binary.BigEndian.PutUint64(buf[i:], r.Uint64())
	}
	var offsets []int64
	var c Chunk
	var i int
	t.Logf("Checking basic persistence and Store expansion.")
	for cs.size <= SegmentSize {
		for i = 0; i < 16; i++ {
			if c, err = cs.Write("rand", buf); err != nil {
				t.Fatalf("Error writing: %v", err)
			}
			offsets = append(offsets, c.Offset())
		}
		if err = cs.Flush(); err != nil {
			t.Fatalf("Error flushing: %v", err)
		}
		i = 0
		err = cs.Each(func(cc Chunk) error {
			if cc.Offset() != offsets[i] {
				t.Fatalf("Expected chunk offset %d, got %d", offsets[i], cc.Offset())
			}
			i++
			return nil
		})
		if err != nil {
			t.Fatalf("Unexpected error %v", err)
		}
		if i != len(offsets) {
			t.Fatalf("Failed to iterate all chunks. Got %d, wanted %d", i, len(offsets))
		}
	}
	t.Logf("Checking close and reopen read-only")
	if err = cs.Close(); err != nil {
		t.Fatalf("Error closing Store: %v", err)
	}
	if err = cs.Open(true); err != nil {
		t.Fatalf("Error reopening Store: %v", err)
	}
	i = 0
	err = cs.Each(func(cc Chunk) error {
		if cc.Offset() != offsets[i] {
			t.Fatalf("Expected chunk offset %d, got %d", offsets[i], cc.Offset())
		}
		i++
		return nil
	})
	if err != nil {
		t.Fatalf("Unexpected error %v", err)
	}
	if i != len(offsets) {
		t.Fatalf("Failed to iterate all chunks. Got %d, wanted %d", i, len(offsets))
	}
	t.Logf("Checking that readonly blocks write ops")
	if c, err = cs.Write("rand", buf); err != ErrReadOnly {
		t.Fatalf("Error writing: %v", err)
	}
	if err = cs.Flush(); err != ErrReadOnly {
		t.Fatalf("Error flushing: %v", err)
	}
	if err = cs.Clear(1); err != ErrReadOnly {
		t.Fatalf("Error clearing: %v", err)
	}
	t.Logf("Checking Clear")
	cs.Close()
	if cs, err = Open(storeFile, false); err != nil {
		t.Fatalf("Error reopening Store: %v", err)
	}
	i = 0
	err = cs.Each(func(cc Chunk) error {
		if cc.Offset() != offsets[i] {
			t.Fatalf("Expected chunk offset %d, got %d", offsets[i], cc.Offset())
		}
		i++
		return nil
	})
	if err != nil {
		t.Fatalf("Unexpected error %v", err)
	}
	if i != len(offsets) {
		t.Fatalf("Failed to iterate all chunks. Got %d, wanted %d", i, len(offsets))
	}
	if err = cs.Clear(1); err != nil {
		t.Fatalf("Error clearing: %v", err)
	}
	offsets = offsets[:0]
	i = 0
	err = cs.Each(func(cc Chunk) error {
		if cc.Offset() != offsets[i] {
			t.Fatalf("Expected chunk offset %d, got %d", offsets[i], cc.Offset())
		}
		i++
		return nil
	})
	if err != nil {
		t.Fatalf("Unexpected error %v", err)
	}
	if i != len(offsets) {
		t.Fatalf("Failed to iterate all chunks. Got %d, wanted %d", i, len(offsets))
	}
	t.Logf("Checking interrupted write")
	for i = 0; i < 16; i++ {
		if c, err = cs.Write("rand", buf); err != nil {
			t.Fatalf("Error writing: %v", err)
		}
	}
	cs.Close()
	if cs, err = Open(storeFile, false); err != nil {
		t.Fatalf("Error reopening Store: %v", err)
	}
	i = 0
	err = cs.Each(func(cc Chunk) error {
		if cc.Offset() != offsets[i] {
			t.Fatalf("Expected chunk offset %d, got %d", offsets[i], cc.Offset())
		}
		i++
		return nil
	})
	if err != nil {
		t.Fatalf("Unexpected error %v", err)
	}
	if i != len(offsets) {
		t.Fatalf("Failed to iterate all chunks. Got %d, wanted %d", i, len(offsets))
	}
}
