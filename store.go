package chunks

import (
	"bytes"
	"errors"
	"fmt"
	"golang.org/x/sys/unix"
	"hash"
	"hash/crc32"
	"os"
	"sync"
	"syscall"
)

const (
	// Alignment of Chunks in the file.  Chunks are aligned on 128 byte boundaries to facilitate file checking.
	Alignment = 128
	// SegmentSize is the size of a segment in the file.  The file is grown by this much when more space is required.
	SegmentSize = 1 << 24
	// ChunkHeaderSize is the size of the Chunk metadata.
	ChunkHeaderSize = 32
	// First is the magic number of the first Chunk in the Store.  It establishes the chunk generation.
	First = `1sT!`
	// Checkpoint is the magic number of a Chunk indicating that a sequence of writes has been committed to disk.
	Checkpoint = `cKpT`
)

var (
	ErrChunkTooShort          = errors.New("Chunk too short")
	ErrChunkTruncated         = errors.New("Chunk truncated")
	ErrChunkCorrupt           = errors.New("Chunk corrupt")
	ErrChunkMoved             = errors.New("Chunk at unexpected offset")
	ErrGenMismatch            = errors.New("Chunk generation mismatch")
	ErrLastCheckpointMismatch = errors.New("Last checkpoint offset changed")
	ErrBadGenStep             = errors.New("Generation step cannot be 0")
	ErrZeroChunk              = errors.New("Zero chunk")
	ErrNeedRecovery           = errors.New("Needs recovery or clear")
	ErrReadOnly               = errors.New("Store is open read-only")
	zeros                     = make([]byte, Alignment)
	pSize                     = int64(os.Getpagesize())
	pMask                     = uint64(pSize - 1)
	crc                       = crc32.MakeTable(crc32.Castagnoli)
	crcPool                   = &sync.Pool{New: func() any { return crc32.New(crc) }}
)

func roundAlign(sz int64) int64 {
	if r := sz % Alignment; r != 0 {
		sz += Alignment - r
	}
	return sz
}

func calcCRC(b []byte) (res uint32) {
	c := crcPool.Get().(hash.Hash32)
	_, _ = c.Write(b)
	res = c.Sum32()
	c.Reset()
	crcPool.Put(c)
	return
}

func retry(err error) bool {
	e, ok := err.(syscall.Errno)
	return ok && e.Temporary()
}

// Store stores Chunks.
type Store struct {
	name                            string
	fi                              *os.File
	m                               []byte // Memory-mapped fi.
	gen                             uint64
	size, writeBarrier, writeOffset int64
	checkPointOffset                int64
	readOnly                        bool
	unsafe                          bool
}

// Size returns the size of the Store.
func (s *Store) Size() int64 {
	return s.size
}

// Close closes a Store by unmapping the backing file then closing it.
// You must call Flush() beforehand to guarantee that all data has been written first.
func (s *Store) Close() (err error) {
	if s.m != nil {
		err = unix.Munmap(s.m)
		s.m = nil
	}
	if s.fi != nil && err == nil {
		err = s.fi.Close()
		s.fi = nil
	}
	return
}

func (s *Store) mmap() (err error) {
	for s.m, err = unix.Mmap(int(s.fi.Fd()),
		0, int(s.size),
		unix.PROT_READ, unix.MAP_SHARED); retry(err); {
	}
	return
}

func (s *Store) checkFirst() (c Chunk, err error) {
	if err = s.CheckAt(0); err == nil {
		c = s.ReadAt(0)
	}
	return
}

func (s *Store) seekToLastCheckpoint() (c Chunk, err error) {
	offset := int64(Alignment)
	var working Chunk
	for {
		if err = s.CheckAt(offset); err != nil {
			if err == ErrZeroChunk {
				err = nil
			}
			break
		}
		working = s.ReadAt(offset)
		if working.Gen() != s.gen {
			break
		}
		if working.Magic() == Checkpoint {
			c = working
		}
		offset = working.NextOffset()
	}
	return
}

// Open a previously-closed Store.  This assumes that the Store had been flushed and closed
// without error. It will error out if the state of the Store has changed.
func (s *Store) Open(readOnly bool) error {
	var err error
	s.fi, err = os.OpenFile(s.name, os.O_RDWR, 0600)
	var st os.FileInfo
	st, err = s.fi.Stat()
	if err != nil {
		s.fi.Close()
		return err
	}
	if st.Size() != s.size {
		s.fi.Close()
		return fmt.Errorf("%s: Size changed from %d to %d while closed.",
			s.name, s.size, st.Size())
	}
	if err = s.mmap(); err != nil {
		return err
	}
	var c Chunk
	c, err = s.checkFirst()
	if err != nil {
		return err
	}
	if c.Gen() != s.gen {
		return ErrGenMismatch
	}
	c, err = s.seekToLastCheckpoint()
	if err != nil {
		return err
	}
	if c.Offset()+Alignment != s.writeOffset ||
		c.Offset() != s.checkPointOffset {
		return ErrLastCheckpointMismatch
	}
	s.readOnly = readOnly
	return s.remap()
}

func (s *Store) remap() (err error) {
	if s.m != nil {
		for err = unix.Munmap(s.m); retry(err); {
		}
		if err != nil {
			return err
		}
	}
	if err = s.mmap(); err != nil {
		return err
	}
	if s.readOnly {
		return nil
	}
	if err != nil {
		return err
	}
	for err = unix.Mprotect(s.m[s.writeBarrier:], unix.PROT_READ|unix.PROT_WRITE); retry(err); {
	}
	return err
}

func (s *Store) checkpoint() {
	cp := Chunk(s.m[s.writeOffset : s.writeOffset+Alignment])
	cp.Fill(Checkpoint, s.gen, ChunkHeaderSize, s.writeOffset)
	copy(cp[ChunkHeaderSize:], zeros)
	s.checkPointOffset = s.writeOffset
	s.writeOffset += Alignment
	copy(s.m[s.writeOffset:], zeros)
}

// Flush writes modified data back to the disk, and updates the memory mapping
// to make committed pages read-only.  This is not complete protection --
// data that straddles the boundary between read-only and read-write pages
// may still be writable.
func (s *Store) Flush() (err error) {
	if s.readOnly {
		return ErrReadOnly
	}
	ft := unix.MS_SYNC
	s.checkpoint()
	if !s.unsafe {
		for err = unix.Msync(s.m[s.writeBarrier:], ft); retry(err); {
		}
	}
	if err != nil {
		return
	}
	nwb := int64(uint64(s.writeOffset) &^ pMask)
	if nwb == s.writeBarrier {
		return
	}
	for err = unix.Mprotect(s.m[s.writeBarrier:nwb], unix.PROT_READ); retry(err); {
	}
	if err != nil {
		return
	}
	s.writeBarrier = nwb
	return
}

func (s *Store) needExpand(offset, writeLen int64) bool {
	return offset+writeLen+(1<<16) >= s.size
}

func (s *Store) expand() (err error) {
	if err = s.fi.Truncate(s.size + SegmentSize); err != nil {
		return
	}
	s.size += SegmentSize
	if err = s.remap(); err != nil {
		return
	}
	return
}

// Expand grows the Store until it is larger than to.
// It will remap the Store at the end, so do not call this concurrent
// with any reads or writes.
func (s *Store) Expand(to int64) error {
	for s.size-(1<<16) < to {
		if err := s.expand(); err != nil {
			return err
		}
	}
	return nil
}

// Shrink will discard extra segments past the end of the write barrier at the
// end of the Store.  It should only be called either just after opening the Store
// or just before closing it.
func (s *Store) Shrink() error {
	for s.writeOffset+SegmentSize > s.size {
		s.size -= SegmentSize
	}
	if err := s.fi.Truncate(s.writeOffset); err != nil {
		return err
	}
	if err := s.fi.Truncate(s.size); err != nil {
		return err
	}
	return s.remap()
}

// Write data as a new Chunk into the Store, expanding the Store as needed.
// You must Flush the Store to ensure data is written to disk and checkpointed.
// Failure to call Flush will result in the accumulated since the last Write being
// discarded the next time the Store is opened.
//
// Write returns the Chunk that was written.  The Chunk will remain valid
// until the next Write needs to expand the underlying Store.  The Offset of the
// chunk will be valid.
func (s *Store) Write(magic string, bufs ...[]byte) (Chunk, error) {
	if s.readOnly {
		return nil, ErrReadOnly
	}
	offset := s.writeOffset
	writeSz := int64(ChunkHeaderSize)
	for i := range bufs {
		writeSz += int64(len(bufs[i]))
	}
	chunkSz := roundAlign(writeSz)
	for s.needExpand(offset, chunkSz) {
		if err := s.expand(); err != nil {
			return nil, err
		}
	}
	buf := Chunk(s.m[offset : offset+chunkSz])
	startWrite := int64(ChunkHeaderSize)
	for i := range bufs {
		copy(buf[startWrite:], bufs[i])
		startWrite += int64(len(bufs[i]))
	}
	if chunkSz > startWrite {
		copy(buf[startWrite:], zeros)
	}
	buf.Fill(magic, s.gen, writeSz, offset)
	s.writeOffset = offset + chunkSz
	return buf, nil
}

func (s *Store) zerosAt(offset int64) bool {
	return bytes.Equal(s.m[offset:offset+128], zeros)
}

// CheckAt checks the Chunk at offset to ensure that it is
// well-formed and that the data has not been altered.
func (s *Store) CheckAt(offset int64) error {
	buf := Chunk(s.m[offset:])
	if len(buf) < Alignment {
		return ErrChunkTooShort
	}
	if s.zerosAt(offset) {
		return ErrZeroChunk
	}
	if buf.Offset() != offset {
		return ErrChunkMoved
	}
	sz := buf.Size()
	if int64(len(buf)) < roundAlign(sz) {
		return ErrChunkTruncated
	}
	buf = buf[:sz]
	if calcCRC(buf[4:]) != buf.Crc() {
		return ErrChunkCorrupt
	}
	return nil
}

// ReadAt reads a Chunk starting at Offset.  It will panic
// if offset does not point at a valid Chunk.  The returned Chunk
// is only valid until Write needs to remap the Store due to
// needing more space on disk.
func (s *Store) ReadAt(offset int64) Chunk {
	if s == nil || s.m == nil || int64(len(s.m)) <= offset {
		panic(ErrChunkCorrupt)
	}
	res := Chunk(s.m[offset:])
	if res.Offset() != offset {
		panic(ErrChunkCorrupt)
	}
	if res.Size() > int64(len(res)) {
		panic(ErrChunkTruncated)
	}
	return res[:res.Size()]
}

func (s *Store) writeFirst(newGen uint64) {
	cp := Chunk(s.m[0:Alignment])
	cp.Fill(First, newGen, ChunkHeaderSize, 0)
	copy(cp[ChunkHeaderSize:], zeros)
	s.writeOffset += Alignment
}

// Clear clears the Store by writing a new First chunk with a new Generation
// and then flushing that chunk.
func (s *Store) Clear(genStep uint64) error {
	if s.readOnly {
		return ErrReadOnly
	}
	if genStep == 0 {
		return ErrBadGenStep
	}
	s.writeOffset = 0
	s.writeBarrier = 0
	s.checkPointOffset = 0
	s.gen = s.gen + genStep
	if err := s.remap(); err != nil {
		return err
	}
	s.writeFirst(s.gen)
	return s.Flush()
}

// OpenAndClear opens a new Store and clears it out all in one operation.
// This should be used when you explicitly want to ignore the data
// in a Store.
func (s *Store) OpenAndClear(genStep uint64) (err error) {
	s.fi, err = os.OpenFile(s.name, os.O_RDWR, 0600)
	var st os.FileInfo
	st, err = s.fi.Stat()
	if err != nil {
		s.fi.Close()
		return err
	}
	if st.Size() != s.size {
		s.fi.Close()
		return fmt.Errorf("%s: Size changed from %d to %d while closed.",
			s.name, s.size, st.Size())
	}
	if err = s.mmap(); err != nil {
		return err
	}
	var c Chunk
	c, err = s.checkFirst()
	if err != nil {
		return err
	}
	if c.Gen() != s.gen {
		return ErrGenMismatch
	}
	return s.Clear(genStep)
}

func (s *Store) needsRecovery() (needRecover bool) {
	offset := int64(128)
	skipped := int64(0)
	var err error
	var c Chunk
	for offset < s.size {
		if err = s.CheckAt(offset); err != nil {
			skipped++
			offset += Alignment
			continue
		}
		c = s.ReadAt(offset)
		if c.Gen() != s.gen {
			skipped++
			offset = c.NextOffset()
			continue
		}
		if c.Magic() == Checkpoint {
			s.checkPointOffset = offset
			if err = s.CheckAt(offset + Alignment); err == ErrZeroChunk {
				if needRecover = skipped > 0; !needRecover {
					s.writeOffset = c.NextOffset()
				}
				return
			}
			s.writeOffset = c.NextOffset()
		}
		offset = c.NextOffset()
	}
	return
}

// Recover clears into, and then copies the valid Chunks in from.
// Invalid Chunks will be skipped.
func Recover(into, from *Store) error {
	if err := into.Clear(1); err != nil {
		return err
	}
	offset := int64(Alignment)
	var c Chunk
	for offset < from.size {
		if err := from.CheckAt(offset); err != nil {
			offset += Alignment
			continue
		}
		if c.Gen() != from.gen || c.Magic() == Checkpoint {
			offset = c.NextOffset()
			continue
		}
		if _, err := into.Write(c.Magic(), c.Payload()); err != nil {
			return err
		}
	}
	return into.Flush()
}

// Each calls thunk once on each flushed Chunk.
// Calling Each concurrent with Write or Clear may result in undefined behaviour.
func (s *Store) Each(thunk func(Chunk) error) (err error) {
	offset := int64(Alignment)
	var c Chunk
	for offset < s.checkPointOffset {
		c = s.ReadAt(offset)
		if c.Magic() != Checkpoint {
			if err = thunk(c); err != nil {
				break
			}
		}
		offset = c.NextOffset()
	}
	return
}

// Open the file and determine if it needs recovery.
func Open(at string, readOnly bool) (*Store, error) {
	res := &Store{name: at, readOnly: readOnly}
	var err error
	res.fi, err = os.OpenFile(at, os.O_RDWR|os.O_CREATE, 0600)
	if err != nil {
		return nil, err
	}
	var st os.FileInfo
	if st, err = res.fi.Stat(); err != nil {
		res.fi.Close()
		return nil, err
	}
	res.size = st.Size()
	if res.size == 0 {
		if err = res.expand(); err != nil {
			return nil, err
		}
		err = res.Clear(1)
		return res, err
	}
	if err = res.mmap(); err != nil {
		return res, err
	}
	var c Chunk
	c, err = res.checkFirst()
	if err == nil {
		res.gen = c.Gen()
	}
	if res.needsRecovery() {
		res.readOnly = true
		return res, ErrNeedRecovery
	}
	if readOnly {
		return res, nil
	}
	if err = res.remap(); err != nil {
		return res, err
	}
	copy(res.m[res.writeOffset:], zeros)
	return res, nil
}

// OpenScratch opens a scratch chunk store.
// Any data in the Store will be cleared, and the Flush operation will not force
// data to disk.
func OpenScratch(at string) (*Store, error) {
	res := &Store{name: at, readOnly: false, unsafe: true}
	var err error
	res.fi, err = os.OpenFile(at, os.O_RDWR|os.O_CREATE, 0600)
	if err != nil {
		return nil, err
	}
	var st os.FileInfo
	if st, err = res.fi.Stat(); err != nil {
		res.fi.Close()
		return nil, err
	}
	res.size = st.Size()
	if res.size == 0 {
		if err = res.expand(); err != nil {
			return nil, err
		}
	}
	err = res.Clear(1)
	return res, err
}
