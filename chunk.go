package chunks

import "encoding/binary"

// Chunk is an individual piece of data stored in the Store.  It always starts
// at an Alignment offset, and has the following structure:
//
//   - [0:4]:   crc32.Castagnoli checksum of the rest of the chunk data.  Used to detect data corruption.
//     Everything after this is checked by the CRC.
//   - [4:8]:   Magic number. First and Checkpoint are used internally for the first chunk
//     and the completed checkpoint marker, all others are available for users.
//   - [8:16]:  Chunk generation.  Generation is established by the first chunk in the sequence, and resetting
//     the Store increments it by the specified value.
//   - [16:24]: The Size of the chunk, including the chunk header.
//   - [24:32]: The Offset in the store the Chunk was written to.
//   - [32:]    Chunk payload.  Opaque to the chunk store.
//
// Two magic strings are used internally by Store: First to establish the current
// Chunk generation, and Checkpoint to indicate that a series of Write calls has been
// Flushed to disk.
type Chunk []byte

// Magic number of the Chunk.
func (c Chunk) Magic() string {
	return string(c[4:8])
}

// Crc is the Castagnoli CRC32 of the rest of the Chunk.
func (c Chunk) Crc() uint32 {
	return binary.BigEndian.Uint32(c[0:4])
}

// Gen is the Generation of the Chunk.  All valid Chunks in the Store
// have the same Gen as the First chunk in the Store.
func (c Chunk) Gen() uint64 {
	return binary.BigEndian.Uint64(c[8:16])
}

// Size of the Chunk, including the header bytes.
func (c Chunk) Size() int64 {
	return int64(binary.BigEndian.Uint64(c[16:24]))
}

// Offset in the Store the chunk was written to.
func (c Chunk) Offset() int64 {
	return int64(binary.BigEndian.Uint64(c[24:32]))
}

// Fill fills in the first 32 bytes of the Chunk with the specified values.
func (c Chunk) Fill(magic string, gen uint64, size, offset int64) {
	copy(c[4:8], magic)
	binary.BigEndian.PutUint64(c[8:16], gen)
	binary.BigEndian.PutUint64(c[16:24], uint64(size))
	binary.BigEndian.PutUint64(c[24:32], uint64(offset))
	binary.BigEndian.PutUint32(c[0:4], calcCRC(c[4:size]))
}

// NextOffset is the offset to the next Chunk in the Store
func (c Chunk) NextOffset() int64 {
	return c.Offset() + roundAlign(c.Size())
}

// Payload is the data stored in the Chunk
func (c Chunk) Payload() []byte {
	return c[ChunkHeaderSize:c.Size()]
}
