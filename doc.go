// Package chunks is an append-only general purpose file-backed memory-mapped blob Store.
//
// # Goals
//
//   - Simple detection and recovery from partial writes and unexpected shutdowns.
//     This is facilitated by using an append-only write strategy, using explicit
//     Checkpoint chunks to mark where Store.Flush was called, by aligning Chunks at
//     128 byte boundaries, and by checksum protections for everything.
//   - Effective write batching with minimal disk write amplification.
//     This is facilitated by using [golang.org/x/sys/unix.Msync] along with Checkpoint
//     chunks to indicate that a series of writes is finished and has been flushed to disk.
//   - Resilience in the face of out-of-space and disk write errors.
//     This is accomplished by growing the Store in 16 megabyte chunks whenever a requested
//     write would result in the Store having less than 64 K free after all data was written
//     to disk.  Data is not copied into the Store until there is enough space free to hold it all.
//     Expanding the Store always involves filling the newly-added space with zeros to catch
//     potential write errors before actual data is written.
//   - Memory safety
//     Since this library uses [golang.org/x/sys/unix.Mmap] as the main file access method,
//     it only makes sections of the Store read-write that must be in order for Store.Write and
//     Store.Flush to do their job.  All other data is mapped read-only.
//
// # Anti-Goals
//
//   - 32 bit system compatibility.
//     Sorry, gotta use this on a 64 bit system.  Being able to mmap very large files in one
//     range keeps offset math down to something that is easy to engineer and understand.
//   - Sorting, searching, or really indexing of any kind.
//     The only method we provide for reading a Chunk is Store.ReadAt, and the only method
//     we provide for discovering Chunks is Store.Each.
//   - Space efficiency.
//     This library never automatically releases disk space once it has been allocated.
//     If you want to do that, you will need to call Shrink()
//
// # Layout
//
// Each Chunk is written to memory at 128 byte aligned offsets to facilitate corruption
// detection and recovery.  The layout is as follows:
//
//	[0:128]:    First chunk.  It establishes the generation number that all subsequent
//	            chunks in the Store must have to be considered valid.
//	[n:m]:     Any number of user specified chunks added to the Store by Write.
//	[m:m+128]: Checkpoint chunk.  Indicates that all previous chunks were Flushed to disk.
//	           If this Chunk and all previous Chunks are valid and have the same generation
//	           as the First chunk, they are considered to be valid.
//	[m+128:m+256]: If this chunk consists of all zeros, that indicates there is no more valid
//	               data in the Store.
//
// On initial creation, the Store is grown to be 16 megabytes in size, the initial First record
// is written to establish the initial Generation (1) and then Flushed to disk.  Subsequent Writes
// and Flushes add data to the Store and checkpoint it until one would leave the Store with less
// than 64K of writable space.  At that point, the Store will be expanded by another 16 megabytes.
//
// On Open of an already created Store, the Chunks will be read to verify that they are
// valid and of the same generation as the First chunk.  Any uncommitted Chunks will be discarded,
// and the presence of any invalid data (Chunks from a different Generation or chunks with an invalid CRC)
// that is covered by a valid Checkpoint will indicate that the Store needs recovery.
//
// Once you no longer need the data in the Store, you can Clear it with a new generation offset, which will
// be added to the current Generation to yield the new Generation.  This will write a new First chunk at
// the beginning of the Store and then Flush the new First chunk.
//
// If Open indicates that a Store needs recovery, you can copy the valid Chunks into a new Store using the
// Recover function.
package chunks
